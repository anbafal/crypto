package com.molotov.cryptocurrency.data.room

import androidx.room.Room
import androidx.test.platform.app.InstrumentationRegistry
import com.molotov.cryptocurrency.data.room.dao.CryptoCurrencyDao
import com.molotov.cryptocurrency.domain.CryptoCurrency
import com.molotov.cryptocurrency.domain.CryptoCurrencyDetail
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Test

@ExperimentalCoroutinesApi
class RoomStorageTest {

    private lateinit var database: AppDatabase
    private lateinit var cryptoDao: CryptoCurrencyDao
    private lateinit var roomStorage: RoomStorage

    @org.junit.Before
    fun setUp() {
        val context = InstrumentationRegistry.getInstrumentation().context
        database =
            Room.inMemoryDatabaseBuilder(context, AppDatabase::class.java).allowMainThreadQueries()
                .build()
        cryptoDao = database.cryptoDao()
        roomStorage = RoomStorage(database)
    }

    @Test
    fun should_save_list_of_crypto_currencies() {
        runBlocking {
            roomStorage.saveCryptoCurrencies(fakeCryptoCurrencies)
            roomStorage.getCryptos(1).onCompletion {
                assertEquals(fakeCryptoCurrencies[0], it)
            }
        }
    }


    @Test
    fun should_save_crypto_details() {
        runBlocking {
            with(roomStorage) {
                // Need to have currency first in db. Assume it will already the case
                saveCryptoCurrencies(fakeCryptoCurrencies)
                saveCryptoDetails(1000, fakeCryptoDetails)
                getCryptosDetails(1000).onCompletion {
                    assertEquals(fakeCryptoCurrencies[1], it)
                }
            }

        }
    }


    @org.junit.After
    fun tearDown() {
    }

    companion object {


        private val fakeCryptoCurrencies = listOf(
            CryptoCurrency(1000, "test0", "fulltest0", "imageUrl0", "symbol0", 1.1, 0),
            CryptoCurrency(1, "test1", "fulltest1", "imageUrl1", "symbol1", 1.2, 1),
            CryptoCurrency(2, "test2", "fulltest2", "imageUrl2", "symbol2", 1.3, 2)
        )

        private val fakeCryptoDetails = listOf(
            CryptoCurrencyDetail("symbol1", 12345.10, 54321.10, 32415.23, 345638.09),
            CryptoCurrencyDetail("symbol2", 123.10, 543.10, 324.23, 3456.09)
        )
    }
}
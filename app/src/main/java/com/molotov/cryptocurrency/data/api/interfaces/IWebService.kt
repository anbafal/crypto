package com.molotov.cryptocurrency.data.api.interfaces

import com.molotov.cryptocurrency.common.Result
import com.molotov.cryptocurrency.data.api.CryptoCurrencyDetailsEntity
import com.molotov.cryptocurrency.data.api.CryptoCurrencyEntity
import kotlinx.coroutines.flow.Flow

interface IWebService {

    suspend fun getCryptos(
        pageNumber: Int,
    ): Flow<Result<List<CryptoCurrencyEntity>>>

    suspend fun getDailyHistory(cryptoSymbol: String): Flow<Result<List<CryptoCurrencyDetailsEntity>>>
}
/*
 *    Copyright 2019 Europcar
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.molotov.cryptocurrency.data.api

import com.google.gson.annotations.SerializedName

/**
 * Classes to represent the json responses.
 */

abstract class Response {
    abstract val message: String
}

data class CryptoCurrencyResponse(
    override val message: String,
    val entities: List<CryptoCurrencyEntity>,
) : Response()

data class CryptoCurrencyEntity(
    @SerializedName("Id")
    val id: Int,
    @SerializedName("Name")
    val name: String,
    @SerializedName("FullName")
    val fullName: String,
    @SerializedName("ImageUrl")
    val imageUrl: String,
    var symbol: String,
    var price: Double,
)

data class CryptoCurrencyDetailsResponse(
    override val message: String,
    val volumes: List<CryptoCurrencyDetailsEntity>,
) : Response()

data class CryptoCurrencyDetailsEntity(
    @SerializedName("volumefrom")
    val volumeFrom: Double,
    @SerializedName("volumeto")
    val volumeTo: Double,
)
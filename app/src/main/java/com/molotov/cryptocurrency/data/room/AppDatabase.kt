package com.molotov.cryptocurrency.data.room

import androidx.room.Database
import androidx.room.RoomDatabase

import com.molotov.cryptocurrency.data.room.dao.CryptoCurrencyDao
import com.molotov.cryptocurrency.data.room.dao.CryptoCurrencyJoinDao
import com.molotov.cryptocurrency.domain.CryptoCurrency
import com.molotov.cryptocurrency.domain.CryptoCurrencyJoin

// todo: Create bean instead using domain directly...first speed and enhance when works as I want
@Database(
    entities = [CryptoCurrency::class, CryptoCurrencyJoin::class],
    version = 1,
    exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun cryptoDao(): CryptoCurrencyDao
    abstract fun cryptoJoinDao(): CryptoCurrencyJoinDao

}

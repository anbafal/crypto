package com.molotov.cryptocurrency.data.room.dao

import androidx.room.Dao
import androidx.room.Query
import com.molotov.cryptocurrency.domain.CryptoCurrencyJoin
import kotlinx.coroutines.flow.Flow


@Dao
interface CryptoCurrencyJoinDao : BaseDao<CryptoCurrencyJoin> {

    // intentionaly return a list of one element. Will be useful to check if should fetch from remote...
    @Query("SELECT * FROM CRYPTO_CURRENCIES_WITH_DETAILS WHERE crypto_id =:cryptoId limit 1")
    fun loadDetailsForCrypto(cryptoId: Int): Flow<CryptoCurrencyJoin>


}
package com.molotov.cryptocurrency.data.api.deserializer

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.molotov.cryptocurrency.data.api.CryptoCurrencyEntity
import com.molotov.cryptocurrency.data.api.CryptoCurrencyResponse
import java.lang.reflect.Type

// custom deserialize as some data need customization to take it
class CryptoCurrencyDeserializer : JsonDeserializer<CryptoCurrencyResponse> {

    override fun deserialize(
        json: JsonElement?,
        typeOfT: Type?,
        context: JsonDeserializationContext?,
    ): CryptoCurrencyResponse {

        val message = json!!.asJsonObject.get("Message").asString
        val data = json!!.asJsonObject.get("Data").asJsonArray
        val cryptosList = arrayListOf<CryptoCurrencyEntity>()

        for (item in data) {

            val coinInfo = item.asJsonObject.get("CoinInfo").asJsonObject
            val price =
                item.asJsonObject.get("RAW")?.asJsonObject?.get("EUR")
                    ?.asJsonObject?.get("PRICE")?.asDouble

            price?.let {
                CryptoCurrencyEntity(coinInfo.get("Id").asInt,
                    coinInfo.get("Name").asString,
                    coinInfo.get("FullName").asString,
                    coinInfo.get("ImageUrl").asString,
                    symbol = coinInfo.get("Name").asString, price)
                    .let {
                        cryptosList.add(it)
                    }
            }
        }

        return CryptoCurrencyResponse(message, cryptosList)
    }


}
package com.molotov.cryptocurrency.data.api.deserializer

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.molotov.cryptocurrency.data.api.CryptoCurrencyDetailsEntity
import com.molotov.cryptocurrency.data.api.CryptoCurrencyDetailsResponse
import java.lang.reflect.Type

class CryptoCurrencyDetailsDeserializer : JsonDeserializer<CryptoCurrencyDetailsResponse> {

    override fun deserialize(
        json: JsonElement?,
        typeOfT: Type?,
        context: JsonDeserializationContext?,
    ): CryptoCurrencyDetailsResponse {

        val message = json!!.asJsonObject.get("Message").asString
        val data = json!!.asJsonObject.get("Data").asJsonObject.get("Data").asJsonArray
        val cryptosDetailsList = arrayListOf<CryptoCurrencyDetailsEntity>()

        for (item in data) {

            val volumeFrom = item.asJsonObject.get("volumefrom").asDouble
            val volumeTo = item.asJsonObject.get("volumeto").asDouble

            CryptoCurrencyDetailsEntity(volumeFrom, volumeTo).let {
                cryptosDetailsList.add(it)
            }
        }

        return CryptoCurrencyDetailsResponse(message, cryptosDetailsList)
    }


}
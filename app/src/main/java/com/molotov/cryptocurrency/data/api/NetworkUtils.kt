package com.molotov.cryptocurrency.data.api

import com.molotov.cryptocurrency.common.Failure
import com.molotov.cryptocurrency.common.Result
import com.molotov.cryptocurrency.data.api.utils.IErrorHandler
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.*
import retrofit2.Response
import timber.log.Timber
import java.io.IOException


typealias NetworkAPIInvoke<T> = suspend () -> Response<T>

@ExperimentalCoroutinesApi
suspend fun <T : Any> performSafeNetworkApiCall(
    allowRetries: Boolean = true,
    numberOfRetries: Int = 2,
    errorHandler: IErrorHandler,
    networkApiCall: NetworkAPIInvoke<T>,
): Flow<Result<T>> {

    //todo: move it in params
    var delayDuration = 300L
    val delayFactor = 2

    return flow {
        val response = networkApiCall()
        if (response.isSuccessful) {

            response.body()?.let {
                emit(Result.OnSuccess(it))
            } ?: emit(Result.OnFailed(Failure.NoBody))
            return@flow
        }
        return@flow
    }.catch { e ->
        emit(Result.OnFailed(errorHandler.getError(e)))
        return@catch
    }.retryWhen { cause, attempt ->
        if (!allowRetries || attempt > numberOfRetries || cause !is IOException) return@retryWhen false
        delay(delayDuration)
        delayDuration *= delayFactor
        return@retryWhen true
    }

    //.flowOn(Dispatchers.IO)

}

/**
 *  Utils method to know how to serve the data.
 *  Even if inline and crossinline seems to be useful for optimization of high order function,
 *  pay attention to debug you have to remove inline and crossinline!!
 */
@ExperimentalCoroutinesApi
inline fun <DB : Any, REMOTE : Any> fetching(
    crossinline fetchFromLocal: () -> Flow<DB>,
    shouldFetchFromRemote: Boolean,
    crossinline fetchFromRemote: suspend () -> Flow<Result<REMOTE>>,
    crossinline saveRemoteData: suspend (REMOTE) -> Unit,
) = flow {

    if (shouldFetchFromRemote) {

        fetchFromRemote()
            .collect { remote ->
                Timber.d("state received directly from remote")
                when (remote) {
                    is Result.OnSuccess -> {
                        saveRemoteData(remote.data)
                        emitAll(fetchFromLocal().map { dbData ->
                            Timber.d("state received success emit from db $dbData")
                            Result.OnSuccess(dbData)
                        })
                    }
                    is Result.OnFailed -> {
                        emitAll(fetchFromLocal().map {
                            Result.OnFailed(remote.failure)
                        })
                    }
                }
            }
    } else {
        Timber.d("state received directly from db")
        emitAll(fetchFromLocal().map { Result.OnSuccess(it) })
    }
}


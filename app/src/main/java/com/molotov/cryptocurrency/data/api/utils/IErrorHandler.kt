package com.molotov.cryptocurrency.data.api.utils

import com.molotov.cryptocurrency.common.Failure


interface IErrorHandler {
    fun getError(error: Throwable): Failure
}
package com.molotov.cryptocurrency.data.room

object RoomConstants {
    const val DATABASE_NAME = "CRYPTOS"
    const val TABLE_CRYPTO_CURRENCY = "CRYPTO_CURRENCIES"
    const val TABLE_CRYPTO_CURRENCY_JOIN = "CRYPTO_CURRENCIES_WITH_DETAILS"
}
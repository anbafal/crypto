package com.molotov.cryptocurrency.data.api.interceptors

import com.molotov.cryptocurrency.BuildConfig
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import java.io.IOException
import javax.inject.Inject

class AuthInterceptor @Inject constructor() : Interceptor {


    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val request: Request = chain.request()
        val authenticatedRequest: Request = request.newBuilder()
            .header("authorization:Apikey", BuildConfig.API_KEY).build()
        return chain.proceed(authenticatedRequest)
    }

}
package com.molotov.cryptocurrency.data

import com.molotov.cryptocurrency.data.api.CryptoCurrencyDetailsEntity
import com.molotov.cryptocurrency.data.api.CryptoCurrencyEntity
import com.molotov.cryptocurrency.domain.CryptoCurrency
import com.molotov.cryptocurrency.domain.CryptoCurrencyDetail

fun CryptoCurrencyEntity.toDomain(indexPage: Int) = CryptoCurrency(id = id,
    name = name,
    fullName = fullName,
    imageUrl = imageUrl,
    symbol = symbol,
    price = price,
    indexInResponse = indexPage)

@JvmName("cryptoEntitiesToDomain")
fun List<CryptoCurrencyEntity>.toDomain(indexPage: Int) = map { it.toDomain(indexPage) }

fun CryptoCurrencyDetailsEntity.toDomain(cryptoSymbol: String) =
    CryptoCurrencyDetail(nameSymbol = cryptoSymbol,
        currentVolume = volumeFrom,
        currentVolumeEur = volumeTo,
        last24Volume = volumeFrom,
        last24VolumeEUR = volumeTo)

@JvmName("cryptoDetailEntitiesToDomain")
fun List<CryptoCurrencyDetailsEntity>.toDomain(cryptoSymbol: String) =
    map { it.toDomain(cryptoSymbol) }

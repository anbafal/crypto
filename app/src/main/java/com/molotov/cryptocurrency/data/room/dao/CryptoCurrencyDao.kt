package com.molotov.cryptocurrency.data.room.dao

import androidx.room.Dao
import androidx.room.Query
import com.molotov.cryptocurrency.domain.CryptoCurrency
import kotlinx.coroutines.flow.Flow


@Dao
interface CryptoCurrencyDao : BaseDao<CryptoCurrency> {

    @Query("SELECT * FROM CRYPTO_CURRENCIES WHERE indexInResponse =:page")
    fun loadCryptos(page: Int): Flow<List<CryptoCurrency>>

    @Query("SELECT * FROM CRYPTO_CURRENCIES WHERE id =:id")
    suspend fun loadCryptoWithId(id: Int): CryptoCurrency


    @Query("DELETE FROM CRYPTO_CURRENCIES")
    fun deleteAll()

}
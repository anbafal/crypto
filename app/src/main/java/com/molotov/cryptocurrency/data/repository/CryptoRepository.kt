package com.molotov.cryptocurrency.data.repository

import com.molotov.cryptocurrency.common.Result
import com.molotov.cryptocurrency.data.api.fetching
import com.molotov.cryptocurrency.data.api.interfaces.IWebService
import com.molotov.cryptocurrency.data.toDomain
import com.molotov.cryptocurrency.domain.CryptoCurrency
import com.molotov.cryptocurrency.domain.CryptoCurrencyJoin
import com.molotov.cryptocurrency.domain.repository.ICryptoRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

@ExperimentalCoroutinesApi
class CryptoRepository @Inject constructor(
    private val webService: IWebService,
    private val localStorage: ILocalStorage,
) : ICryptoRepository {


    override suspend fun topListCryptos(page: Int): Flow<Result<List<CryptoCurrency>>> {
        return fetching(
            fetchFromRemote = {
                webService.getCryptos(page).map {
                    //Timber.d("set page for domain $page")
                    when (it) {
                        // todo: cast to bean instead
                        is Result.OnSuccess -> Result.OnSuccess(it.data.toDomain(page))
                        is Result.OnFailed -> Result.OnFailed(it.failure)
                    }
                }
            },
            saveRemoteData = { localStorage.saveCryptoCurrencies(it) },
            fetchFromLocal = { localStorage.getCryptos(page) },
            shouldFetchFromRemote = !localStorage.hasCryptosForPage(page)
        )
    }

    override suspend fun dailyHistorical(
        cryptoId: Int,
        cryptoSymbol: String,
    ): Flow<Result<CryptoCurrencyJoin>> {
        return fetching(
            fetchFromRemote = {
                webService.getDailyHistory(cryptoSymbol = cryptoSymbol).map {
                    when (it) {
                        is Result.OnSuccess -> Result.OnSuccess(it.data.toDomain(cryptoSymbol))
                        is Result.OnFailed -> Result.OnFailed(it.failure)
                    }
                }
            },
            saveRemoteData = { localStorage.saveCryptoDetails(cryptoId, it) },
            fetchFromLocal = { localStorage.getCryptosDetails(cryptoId) },
            shouldFetchFromRemote = !localStorage.hasDetailsForCrypto(cryptoId)
        )
    }
}
package com.molotov.cryptocurrency.data.api

import com.molotov.cryptocurrency.common.Failure
import com.molotov.cryptocurrency.common.Result
import com.molotov.cryptocurrency.data.api.interfaces.CryptoCompareApi
import com.molotov.cryptocurrency.data.api.interfaces.IWebService
import com.molotov.cryptocurrency.data.api.utils.IErrorHandler
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.transformLatest
import timber.log.Timber
import javax.inject.Inject

@ExperimentalCoroutinesApi
class RetrofitService @Inject constructor(
    private val api: CryptoCompareApi,
    private val errorHandler: IErrorHandler,
) : IWebService {

    override suspend fun getCryptos(pageNumber: Int): Flow<Result<List<CryptoCurrencyEntity>>> {
        return performSafeNetworkApiCall(errorHandler = errorHandler) {
            api.getTopListCryptos(
                page = pageNumber
            )
        }.transformLatest {
            Timber.d("state received $it")
            when (it) {
                is Result.OnSuccess -> {
                    if (it.data.entities.isNotEmpty()) {
                        emit(Result.OnSuccess(it.data.entities))
                    } else {
                        emit(Result.OnFailed(Failure.Network(it.data.message)))
                    }
                }
                is Result.OnFailed -> emit(Result.OnFailed(it.failure))
            }
        }//.flowOn(Dispatchers.IO)//.catch { Result.OnFailed(Failure.Network) }
    }

    override suspend fun getDailyHistory(cryptoSymbol: String): Flow<Result<List<CryptoCurrencyDetailsEntity>>> {

        return performSafeNetworkApiCall(errorHandler = errorHandler) {
            api.getHistoDay(cryptoSymbol = cryptoSymbol)
        }.transformLatest {
            when (it) {
                is Result.OnSuccess -> {
                    if (it.data.volumes.isNotEmpty()) {
                        emit(Result.OnSuccess(it.data.volumes))
                    } else {
                        emit(Result.OnFailed(Failure.Network(it.data.message)))
                    }
                }
                is Result.OnFailed -> emit(Result.OnFailed(it.failure))
            }
        }
    }
}
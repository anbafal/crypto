package com.molotov.cryptocurrency.data.api.interfaces

import com.molotov.cryptocurrency.data.api.CryptoCurrencyDetailsResponse
import com.molotov.cryptocurrency.data.api.CryptoCurrencyResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * API communication with https://min-api.cryptocompare.com/
 *
 */
interface CryptoCompareApi {

    companion object {
        //Todo: move in build
        const val BASE_URL = "https://min-api.cryptocompare.com/"
        private const val GET_ALL_CRYPTO_CURRENCIES = "/data/top/mktcapfull"
        private const val GET_CRYPTO_CURRENCY_VOLUME = "/data/v2/histoday"
    }

    //todo: default params for this app could be insert directly in link above
    @GET(GET_ALL_CRYPTO_CURRENCIES)
    suspend fun getTopListCryptos(
        @Query("page") page: Int,
        @Query("tsym") devise: String = "EUR",
        @Query("limit") limit: Int = 20,
    ): Response<CryptoCurrencyResponse>

    @GET(GET_CRYPTO_CURRENCY_VOLUME)
    suspend fun getHistoDay(
        @Query("fsym") cryptoSymbol: String,
        @Query("tsym") currencySymbol: String = "EUR",
        @Query("limit") limit: Int = 1,
        @Query("aggregate") aggregate: Int = 1,
    ): Response<CryptoCurrencyDetailsResponse>

}
package com.molotov.cryptocurrency.data.api.utils

import com.molotov.cryptocurrency.common.Failure
import retrofit2.HttpException
import java.io.IOException
import java.net.HttpURLConnection

class GenericApiErrorHandler : IErrorHandler {
    override fun getError(error: Throwable): Failure {
        error.printStackTrace()
        return when (error) {
            is HttpException -> {
                when (error.code()) {
                    HttpURLConnection.HTTP_NOT_FOUND -> Failure.NotFound
                    HttpURLConnection.HTTP_FORBIDDEN -> Failure.AccessDenied
                    HttpURLConnection.HTTP_UNAVAILABLE -> Failure.ServiceUnavailable
                    else -> Failure.Network(error.message)
                }
            }
            is IOException -> Failure.Network(error.message)
            else -> Failure.Network(error.message)
        }
    }
}
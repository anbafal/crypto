package com.molotov.cryptocurrency.data.room

import androidx.room.withTransaction
import com.molotov.cryptocurrency.data.repository.ILocalStorage
import com.molotov.cryptocurrency.domain.CryptoCurrency
import com.molotov.cryptocurrency.domain.CryptoCurrencyDetail
import com.molotov.cryptocurrency.domain.CryptoCurrencyJoin
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.firstOrNull
import timber.log.Timber
import javax.inject.Inject

class RoomStorage @Inject constructor(private val database: AppDatabase) : ILocalStorage {
    override suspend fun saveCryptoCurrencies(cryptos: List<CryptoCurrency>) {
        //in case of refresh be sure to not keep unuseful data
        // todo: Having a usecase to call it when refresh from vm
        if (cryptos.any { it.indexInResponse == 0 }) {
            database.withTransaction {
                //database.cryptoDao().deleteAll()
                database.clearAllTables()
                database.cryptoDao().insertAll(cryptos)
            }
        }
        database.cryptoDao().insertAll(cryptos)
    }

    override fun getCryptos(page: Int): Flow<List<CryptoCurrency>> =
        database.cryptoDao().loadCryptos(page)

    override suspend fun hasCryptosForPage(page: Int): Boolean {
        return database.cryptoDao().loadCryptos(page).firstOrNull()?.isEmpty() ?: false
    }

    override suspend fun saveCryptoDetails(
        cryptoId: Int,
        cryptoDetails: List<CryptoCurrencyDetail>,
    ) {
        Timber.d("crypto join saving")
        with(database) {
            withTransaction {
                val cryptoCurrency = this.cryptoDao().loadCryptoWithId(cryptoId)
                // With network request done, if I understand  well, we should take details of current date
                this.cryptoJoinDao().insert(CryptoCurrencyJoin(cryptoCurrency, cryptoDetails[1]))
            }
        }

    }

    override fun getCryptosDetails(cryptoId: Int): Flow<CryptoCurrencyJoin> =
        database.cryptoJoinDao().loadDetailsForCrypto(cryptoId)

    override suspend fun hasDetailsForCrypto(cryptoId: Int): Boolean {
        //todo: investigate
        //return getCryptosDetails(cryptoId).count{it?.cryptoCurrency?.id == cryptoId} == 1
        return true
    }


}
package com.molotov.cryptocurrency.data.repository

import com.molotov.cryptocurrency.domain.CryptoCurrency
import com.molotov.cryptocurrency.domain.CryptoCurrencyDetail
import com.molotov.cryptocurrency.domain.CryptoCurrencyJoin
import kotlinx.coroutines.flow.Flow


interface ILocalStorage {
    suspend fun saveCryptoCurrencies(cryptos: List<CryptoCurrency>)

    fun getCryptos(page: Int): Flow<List<CryptoCurrency>>

    suspend fun hasCryptosForPage(page: Int): Boolean

    suspend fun saveCryptoDetails(
        cryptoId: Int,
        cryptoDetails: List<CryptoCurrencyDetail>,
    )

    fun getCryptosDetails(cryptoId: Int): Flow<CryptoCurrencyJoin>

    suspend fun hasDetailsForCrypto(cryptoId: Int): Boolean
}

package com.molotov.cryptocurrency.common


sealed class Result<out T : Any> {
    data class OnSuccess<out T : Any>(val data: T) : Result<T>()
    data class OnFailed(val failure: Failure) : Result<Nothing>()
}

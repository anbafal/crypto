package com.molotov.cryptocurrency.common

sealed class Failure {
    object AccessDenied : Failure()
    object ServiceUnavailable : Failure()
    data class Network(val message: String?) : Failure()
    object NoBody : Failure()
    object NotFound : Failure()
}
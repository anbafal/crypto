package com.molotov.cryptocurrency.di

import com.google.gson.GsonBuilder
import com.molotov.cryptocurrency.data.api.CryptoCurrencyDetailsResponse
import com.molotov.cryptocurrency.data.api.CryptoCurrencyResponse
import com.molotov.cryptocurrency.data.api.RetrofitService
import com.molotov.cryptocurrency.data.api.deserializer.CryptoCurrencyDeserializer
import com.molotov.cryptocurrency.data.api.deserializer.CryptoCurrencyDetailsDeserializer
import com.molotov.cryptocurrency.data.api.interceptors.AuthInterceptor
import com.molotov.cryptocurrency.data.api.interceptors.CacheInterceptor
import com.molotov.cryptocurrency.data.api.interfaces.CryptoCompareApi
import com.molotov.cryptocurrency.data.api.interfaces.IWebService
import com.molotov.cryptocurrency.data.api.utils.IErrorHandler
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    @Provides
    @Singleton
    fun provideRetrofitService(api: CryptoCompareApi, errorHandler: IErrorHandler): IWebService =
        RetrofitService(api, errorHandler)

    @Singleton
    @Provides
    fun provideCryptoService(retrofit: Retrofit): CryptoCompareApi {
        return retrofit.create(CryptoCompareApi::class.java)
    }

    @Provides
    @Singleton
    fun providesRetrofit(
        gsonConverterFactory: GsonConverterFactory,
        okHttpClient: OkHttpClient,
    ): Retrofit {
        return Retrofit.Builder().baseUrl(CryptoCompareApi.BASE_URL)
            .addConverterFactory(gsonConverterFactory)
            .client(okHttpClient)
            .build()
    }

    @Provides
    @Singleton
    fun providesOkHttpClient(
        authInterceptor: AuthInterceptor,
        cacheInterceptor: CacheInterceptor,
    ): OkHttpClient {
        val client = OkHttpClient.Builder()
            .connectTimeout(20, TimeUnit.SECONDS)
            .writeTimeout(60, TimeUnit.SECONDS)
            .readTimeout(20, TimeUnit.SECONDS)

        //todo:move outside httpLoggingInterceptor
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY

        client.addNetworkInterceptor(authInterceptor).addNetworkInterceptor(cacheInterceptor)
            .addInterceptor(interceptor)

        return client.build()
    }

    @Provides
    @Singleton
    fun providesCacheInterceptor(): CacheInterceptor = CacheInterceptor()

    @Provides
    @Singleton
    fun providesGsonConverterFactory(
        cryptoCurrencyDeserializer: CryptoCurrencyDeserializer,
        cryptoCurrencyDetailsDeserializer: CryptoCurrencyDetailsDeserializer,
    ): GsonConverterFactory {
        //todo: could extract gson builder
        val gsonBuilder = GsonBuilder()
        gsonBuilder
            .registerTypeAdapter(CryptoCurrencyResponse::class.java,
                cryptoCurrencyDeserializer)
            .registerTypeAdapter(
                CryptoCurrencyDetailsResponse::class.java,
                cryptoCurrencyDetailsDeserializer,
            )
        return GsonConverterFactory.create(gsonBuilder.create())
    }

    @Provides
    @Singleton
    fun providesCryptoCurrencyDeserializer() = CryptoCurrencyDeserializer()

    @Provides
    @Singleton
    fun providesCryptoCurrencyDetailsDeserializer() = CryptoCurrencyDetailsDeserializer()


}
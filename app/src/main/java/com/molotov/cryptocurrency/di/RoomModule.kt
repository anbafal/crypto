package com.molotov.cryptocurrency.di

import android.content.Context
import androidx.room.Room
import com.molotov.cryptocurrency.data.repository.ILocalStorage
import com.molotov.cryptocurrency.data.room.AppDatabase
import com.molotov.cryptocurrency.data.room.RoomConstants
import com.molotov.cryptocurrency.data.room.RoomStorage
import com.molotov.cryptocurrency.data.room.dao.CryptoCurrencyDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RoomModule {

    @Provides
    @Singleton
    fun providesRoomDatabase(@ApplicationContext context: Context): AppDatabase {
        return Room.databaseBuilder(context, AppDatabase::class.java, RoomConstants.DATABASE_NAME)
            .build()
    }

    @Provides
    @Singleton
    fun providesCryptoDao(database: AppDatabase): CryptoCurrencyDao {
        return database.cryptoDao()
    }

    @Provides
    @Singleton
    fun providesCryptJoinDao(database: AppDatabase) = database.cryptoJoinDao()


    @Provides
    @Singleton
    fun providesLocalStorage(appDatabase: AppDatabase): ILocalStorage {
        return RoomStorage(appDatabase)
    }


}
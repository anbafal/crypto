package com.molotov.cryptocurrency.di

import com.molotov.cryptocurrency.data.api.interfaces.IWebService
import com.molotov.cryptocurrency.data.api.utils.GenericApiErrorHandler
import com.molotov.cryptocurrency.data.api.utils.IErrorHandler
import com.molotov.cryptocurrency.data.repository.CryptoRepository
import com.molotov.cryptocurrency.data.repository.ILocalStorage
import com.molotov.cryptocurrency.domain.repository.ICryptoRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
object MainModule {

    @Provides
    @Singleton
    fun provideCryptoRepository(
        webService: IWebService,
        localStorage: ILocalStorage,
    ): ICryptoRepository = CryptoRepository(webService, localStorage)

    @Provides
    @Singleton
    fun provideErrorHandler(): IErrorHandler = GenericApiErrorHandler()

    @DefaultDispatcher
    @Provides
    fun providesDefaultDispatcher(): CoroutineDispatcher = Dispatchers.Default

    @IoDispatcher
    @Provides
    fun providesIoDispatcher(): CoroutineDispatcher = Dispatchers.IO

    @MainDispatcher
    @Provides
    fun providesMainDispatcher(): CoroutineDispatcher = Dispatchers.Main

}
package com.molotov.cryptocurrency.features.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavDirections
import com.molotov.cryptocurrency.features.utils.NavigationCommand
import com.molotov.cryptocurrency.features.utils.SingleLiveEvent
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

abstract class BaseViewModel(private val savedStateHandle: SavedStateHandle) : ViewModel() {


    private val _navigationCommand: SingleLiveEvent<NavigationCommand> by lazy {
        SingleLiveEvent()
    }
    internal val navigationCommand: LiveData<NavigationCommand>
        get() = _navigationCommand



    protected fun navigate(directions: NavDirections?) {
        if (directions != null) _navigationCommand.postValue(NavigationCommand.To(directions))
        else _navigationCommand.postValue(
            NavigationCommand.ToRoot
        )

    }

    internal fun execute(block: suspend CoroutineScope.() -> Unit): Job {
        return viewModelScope.launch {
            block()
        }
    }
}
package com.molotov.cryptocurrency.features.poll

import kotlinx.coroutines.flow.Flow

interface Poller<T> {
    fun poll(delay: Long): Flow<T>
    fun close()
}
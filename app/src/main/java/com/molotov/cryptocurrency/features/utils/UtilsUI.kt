package com.molotov.cryptocurrency.features.utils

import android.content.Context
import android.widget.ImageView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.molotov.cryptocurrency.R
import com.molotov.cryptocurrency.common.Failure
import com.molotov.cryptocurrency.common.Result
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.*
import timber.log.Timber


@ExperimentalCoroutinesApi
suspend fun <T : Any> viewStateFlow(
    dispatcher: CoroutineDispatcher,
    ioOperation: suspend () -> Flow<Result<T>>,
) =
    flow {
        emit(ViewState.Loading(true))
        ioOperation().map {
            Timber.d("state received $it")
            when (it) {
                is Result.OnSuccess -> ViewState.RenderSuccess(it.data)
                is Result.OnFailed -> ViewState.RenderFailure(it.failure)
            }
        }.collect {
            emit(it)
            emit(ViewState.Loading(false))
        }
    }.flowOn(dispatcher)


// Extensions

fun Context.toast(message: CharSequence) =
    Toast.makeText(this, message, Toast.LENGTH_LONG).show()

fun ImageView.loadUrl(url: String) {
    Glide.with(context)
        .load(url)
        .placeholder(R.drawable.ic_launcher_background)
        .into(this)
}

fun Context.displayFailure(error: Failure): String {
    ErrorResolver.getErrorResId(error).let {
        return when (error) {
            is Failure.Network -> getString(it, error.message)
            is Failure.AccessDenied, Failure.NoBody, Failure.ServiceUnavailable, Failure.NotFound -> getString(
                it)

        }
    }

}



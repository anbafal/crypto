package com.molotov.cryptocurrency.features.cryptocurrencies

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.molotov.cryptocurrency.R
import com.molotov.cryptocurrency.features.utils.loadUrl
import kotlinx.android.synthetic.main.crypto_currency_item.view.*

class CryptoAdapter(private val cryptoClickListener: CryptoClickListener) :
    ListAdapter<CryptoCurrencyUI, CryptoAdapter.ItemViewholder>(DiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewholder {
        return ItemViewholder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.crypto_currency_item, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ItemViewholder, position: Int) {
        holder.bind(getItem(position), cryptoClickListener)
    }

    class ItemViewholder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: CryptoCurrencyUI, clickListener: CryptoClickListener) = with(itemView) {

            name.text = item.name
            value.text = item.price.toString()
            //todo: investigate why url don't work...same in the webbrowser
            logo.loadUrl(item.imageUrl)

            setOnClickListener {
                clickListener.onClickCrypto(item)
            }
        }
    }
}

class DiffCallback : DiffUtil.ItemCallback<CryptoCurrencyUI>() {
    override fun areItemsTheSame(oldItem: CryptoCurrencyUI, newItem: CryptoCurrencyUI): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: CryptoCurrencyUI, newItem: CryptoCurrencyUI): Boolean {
        return oldItem == newItem
    }

}

interface CryptoClickListener {
    fun onClickCrypto(cryptoCurrencyUI: CryptoCurrencyUI)
}
package com.molotov.cryptocurrency.features.utils

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer

interface ObserveUtils {
    fun <T> LiveData<T>.observe(observer: Observer<T>) {
        this.observe(getObserveLifecycleOwner(), observer)
    }

    fun getObserveLifecycleOwner(): LifecycleOwner
}
package com.molotov.cryptocurrency.features.cryptodetails

import com.molotov.cryptocurrency.features.cryptocurrencies.CryptoCurrencyUI

data class CryptoJoinUI(
    val cryptoCurrencyUI: CryptoCurrencyUI,
    val currentVolume: Double = 0.0,
    val last24Volume: Double = 0.0,
    val currentVolumeEur: Double = 0.0,
    val last24VolumeEUR: Double = 0.0,
)
package com.molotov.cryptocurrency.features.cryptocurrencies

import androidx.lifecycle.LiveData
import androidx.lifecycle.SavedStateHandle
import com.molotov.cryptocurrency.di.IoDispatcher
import com.molotov.cryptocurrency.domain.CryptoCurrency
import com.molotov.cryptocurrency.domain.usecase.GetCryptosCurrencies
import com.molotov.cryptocurrency.features.base.BaseViewModel
import com.molotov.cryptocurrency.features.toUI
import com.molotov.cryptocurrency.features.utils.ValueKeeperLiveData
import com.molotov.cryptocurrency.features.utils.ViewState
import com.molotov.cryptocurrency.features.utils.viewStateFlow
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import timber.log.Timber
import javax.inject.Inject

@ExperimentalCoroutinesApi
@HiltViewModel
class CryptoCurrenciesViewModel @Inject constructor(
    private val savedStateHandle: SavedStateHandle,
    private val getCryptosCurrencies: GetCryptosCurrencies,
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher,
) :
    BaseViewModel(savedStateHandle) {


    private val cryptoList = ArrayList<CryptoCurrencyUI>()

    private val _cryptosLiveData: ValueKeeperLiveData<ViewState<List<CryptoCurrencyUI>>> by lazy {
        ValueKeeperLiveData()
    }
    internal val cryptosLiveData: LiveData<ViewState<List<CryptoCurrencyUI>>>
        get() = _cryptosLiveData


    fun loadCryptos(page: Int = 0) {
        Timber.d("at page $page")
        execute {
            viewStateFlow(ioDispatcher) {
                getCryptosCurrencies(page)
            }.collect { state ->
                Timber.d("state received $state")
                when (state) {
                    is ViewState.Loading -> _cryptosLiveData.postValue(state)
                    is ViewState.RenderFailure -> _cryptosLiveData.postValue(state)
                    is ViewState.RenderSuccess<List<CryptoCurrency>> -> {
                        cryptoList.addAll(state.output.toUI())
                        _cryptosLiveData.postValue(ViewState.RenderSuccess(cryptoList.distinct()))
                    }
                }
            }
        }
    }

    //todo: fix bug on pagination when refresh or go back from details fragment
    fun onRefresh() {
        // savedStateHandle.set(KEY_PAGE, 0)
        cryptoList.clear()
        loadCryptos()
    }


    fun onLoadMore(page: Int) {
        //todo: save page in savedStateHandle and use it instead of page for calling
        //savedStateHandle.set(KEY_PAGE, page)
        loadCryptos(page)
    }

    fun onSelectCrypto(cryptoUI: CryptoCurrencyUI) {
        Timber.d("On select crypto $cryptoUI")
        navigate(CryptoCurrenciesFragmentDirections
            .actionCryptoCurrenciesFragmentToCryptoCurrencyDetailsFragment(cryptoUI))
    }

    companion object {
        private const val KEY_PAGE = "KEY_PAGE"
    }
}

package com.molotov.cryptocurrency.features.cryptocurrencies

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CryptoCurrencyUI(
    val id: Int,
    val name: String,
    val fullName: String,
    val imageUrl: String,
    val symbol: String,
    val price: Double,
    val indexInResponse: Int,
) : Parcelable
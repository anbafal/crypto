package com.molotov.cryptocurrency.features.utils

import com.molotov.cryptocurrency.R
import com.molotov.cryptocurrency.common.Failure

object ErrorResolver {

    fun getErrorResId(error: Failure): Int = when (error) {
        is Failure.Network -> R.string.error_no_network
        is Failure.AccessDenied -> R.string.error_access_denied
        is Failure.NoBody -> R.string.error_no_body
        is Failure.ServiceUnavailable -> R.string.error_service_unavailable
        is Failure.NotFound -> R.string.error_not_found

    }
}
package com.molotov.cryptocurrency.features.cryptodetails

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import com.molotov.cryptocurrency.databinding.FragmentCryptoCurrencyDetailsBinding
import com.molotov.cryptocurrency.features.base.BaseFragment
import com.molotov.cryptocurrency.features.utils.ViewState
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi
import timber.log.Timber

@ExperimentalCoroutinesApi
@AndroidEntryPoint
class CryptoCurrencyDetailsFragment :
    BaseFragment<FragmentCryptoCurrencyDetailsBinding, CryptoDetailViewModel>() {

    init {
        lifecycleScope.launchWhenCreated {
            getViewModel().loadDetails(navigationArgs.crypto)
        }
    }

    private val navigationArgs: CryptoCurrencyDetailsFragmentArgs by navArgs()

    private val vm: CryptoDetailViewModel by viewModels()

    override fun getViewModel() = vm

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = FragmentCryptoCurrencyDetailsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun setUpMainObservers() {
        super.setUpMainObservers()
        getViewModel().cryptoDetailsLiveData.observe(Observer(this@CryptoCurrencyDetailsFragment::observedCryptoDetails))

    }

    private fun observedCryptoDetails(state: ViewState<CryptoJoinUI>) {
        when (state) {
            is ViewState.Loading -> Timber.d("laoding")
            is ViewState.RenderSuccess -> {
                with(binding) {
                    cryptoName.text = state.output.cryptoCurrencyUI.name
                    cryptoPrice.text = state.output.cryptoCurrencyUI.price.toString()
                }

            }
            is ViewState.RenderFailure -> displayError(state.failure)
        }
    }


}

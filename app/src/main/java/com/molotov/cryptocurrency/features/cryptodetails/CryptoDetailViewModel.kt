package com.molotov.cryptocurrency.features.cryptodetails

import androidx.lifecycle.LiveData
import androidx.lifecycle.SavedStateHandle
import com.molotov.cryptocurrency.di.IoDispatcher
import com.molotov.cryptocurrency.domain.CryptoCurrencyJoin
import com.molotov.cryptocurrency.domain.usecase.GetCryptoCurrencyDetails
import com.molotov.cryptocurrency.features.base.BaseViewModel
import com.molotov.cryptocurrency.features.cryptocurrencies.CryptoCurrencyUI
import com.molotov.cryptocurrency.features.toUI
import com.molotov.cryptocurrency.features.utils.ValueKeeperLiveData
import com.molotov.cryptocurrency.features.utils.ViewState
import com.molotov.cryptocurrency.features.utils.viewStateFlow
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import timber.log.Timber
import javax.inject.Inject

@ExperimentalCoroutinesApi
@HiltViewModel
class CryptoDetailViewModel @Inject constructor(
    private val savedStateHandle: SavedStateHandle,
    private val getCryptoCurrencyDetails: GetCryptoCurrencyDetails,
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher,
) :
    BaseViewModel(savedStateHandle) {

    private val _cryptoDetailsLiveData: ValueKeeperLiveData<ViewState<CryptoJoinUI>> by lazy {
        ValueKeeperLiveData<ViewState<CryptoJoinUI>>()
    }
    internal val cryptoDetailsLiveData: LiveData<ViewState<CryptoJoinUI>>
        get() = _cryptoDetailsLiveData


    fun loadDetails(cryptoCurrencyUI: CryptoCurrencyUI) {

        // Before getting the data from network, we already know
        // some basic information we can set up this info
        _cryptoDetailsLiveData.postValue(ViewState.RenderSuccess(CryptoJoinUI(cryptoCurrencyUI)))
        execute {
            viewStateFlow(ioDispatcher) {
                getCryptoCurrencyDetails(GetCryptoCurrencyDetails.Params(cryptoCurrencyUI.id,
                    cryptoCurrencyUI.symbol))
            }.collect { state ->
                Timber.d("state join $state")
                when (state) {
                    is ViewState.Loading -> _cryptoDetailsLiveData.postValue(state)
                    is ViewState.RenderFailure -> _cryptoDetailsLiveData.postValue(state)
                    is ViewState.RenderSuccess<CryptoCurrencyJoin> -> {
                        Timber.d("join ${state.output.toUI()}")
                        _cryptoDetailsLiveData.postValue(ViewState.RenderSuccess(state.output.toUI()))
                    }
                }
            }
        }
    }


}
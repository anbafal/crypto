package com.molotov.cryptocurrency.features.base

import android.os.Bundle
import android.view.View
import androidx.annotation.CallSuper
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.viewbinding.ViewBinding
import com.molotov.cryptocurrency.R
import com.molotov.cryptocurrency.common.Failure
import com.molotov.cryptocurrency.features.utils.NavigationCommand
import com.molotov.cryptocurrency.features.utils.ObserveUtils
import com.molotov.cryptocurrency.features.utils.displayFailure
import com.molotov.cryptocurrency.features.utils.toast


abstract class BaseFragment<VDB : ViewBinding, VM : BaseViewModel?> : Fragment(),
    ObserveUtils {

    protected var _binding: VDB? = null


    protected val binding get() = _binding!!

    protected open fun getViewModel(): VM? = null
    private val navController: NavController by lazy { findNavController() }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpMainObservers()
    }


    private fun observedNavigationCommand(navigationCommand: NavigationCommand) {
        when (navigationCommand) {
            is NavigationCommand.To -> navController.navigate(navigationCommand.directions)
            is NavigationCommand.Back -> navController.popBackStack()
            is NavigationCommand.BackTo -> navController.navigate(navigationCommand.destinationId)
            is NavigationCommand.ToRoot -> navController.popBackStack(R.id.cryptoCurrenciesFragment,
                false)
        }
    }

    /**
     * To declare all observable live data in fragment
     */
    @CallSuper
    open fun setUpMainObservers() {
        getViewModel().let {
            it?.navigationCommand?.observe(Observer(this@BaseFragment::observedNavigationCommand))
        }
    }

    fun displayError(failure: Failure) {
        requireContext().apply {
            toast(this.displayFailure(failure))
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun getObserveLifecycleOwner(): LifecycleOwner {
        return viewLifecycleOwner
    }


}
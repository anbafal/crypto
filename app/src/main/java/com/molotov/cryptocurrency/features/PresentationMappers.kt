package com.molotov.cryptocurrency.features

import com.molotov.cryptocurrency.data.api.interfaces.CryptoCompareApi
import com.molotov.cryptocurrency.domain.CryptoCurrency
import com.molotov.cryptocurrency.domain.CryptoCurrencyJoin
import com.molotov.cryptocurrency.features.cryptocurrencies.CryptoCurrencyUI
import com.molotov.cryptocurrency.features.cryptodetails.CryptoJoinUI


fun CryptoCurrency.toUI() = CryptoCurrencyUI(id = id,
    name = name,
    fullName = fullName,
    imageUrl = CryptoCompareApi.BASE_URL + imageUrl.drop(1),
    symbol = symbol,
    price = price,
    indexInResponse = 0)

@JvmName("cryptoDomainToUi")
fun List<CryptoCurrency>.toUI() = map { it.toUI() }


fun CryptoCurrencyJoin.toUI() = CryptoJoinUI(
    cryptoCurrency.toUI(),
    cryptoCurrencyDetail.currentVolume,
    cryptoCurrencyDetail.last24Volume,
    cryptoCurrencyDetail.currentVolumeEur,
    cryptoCurrencyDetail.last24VolumeEUR
)
package com.molotov.cryptocurrency.features.cryptocurrencies

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.molotov.cryptocurrency.databinding.FragmentCryptoCurrenciesBinding
import com.molotov.cryptocurrency.features.base.BaseFragment
import com.molotov.cryptocurrency.features.utils.EndlessRecyclerViewScrollListener
import com.molotov.cryptocurrency.features.utils.ViewState
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi
import timber.log.Timber


@ExperimentalCoroutinesApi
@AndroidEntryPoint
class CryptoCurrenciesFragment :
    BaseFragment<FragmentCryptoCurrenciesBinding, CryptoCurrenciesViewModel>(),
    SwipeRefreshLayout.OnRefreshListener, CryptoClickListener {


    private val vm: CryptoCurrenciesViewModel by viewModels()
    private lateinit var cryptoAdapter: CryptoAdapter

    override fun getViewModel() = vm

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = FragmentCryptoCurrenciesBinding.inflate(inflater, container, false)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
        getViewModel().loadCryptos()
    }


    //todo: don't keep only one method to init
    private fun initRecyclerView() {
        with(binding) {

            rvCryptoCurrencies.apply {
                setHasFixedSize(true)
                layoutManager = LinearLayoutManager(requireContext())
                cryptoAdapter = CryptoAdapter(this@CryptoCurrenciesFragment)
                cryptoAdapter.stateRestorationPolicy =
                    RecyclerView.Adapter.StateRestorationPolicy.PREVENT_WHEN_EMPTY
                adapter = cryptoAdapter

                val endlessRecyclerViewScrollListener = object :
                    EndlessRecyclerViewScrollListener(this.layoutManager as LinearLayoutManager) {
                    override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView) {
                        Timber.d("page $page")
                        getViewModel().onLoadMore(page)
                    }
                }
                //endlessRecyclerViewScrollListener.resetState()
                addOnScrollListener(endlessRecyclerViewScrollListener)

                val dividerItemDecoration = DividerItemDecoration(context,
                    (layoutManager as LinearLayoutManager).orientation)
                addItemDecoration(dividerItemDecoration);
            }

            swipeRefresh.setOnRefreshListener(this@CryptoCurrenciesFragment)

        }

    }

    override fun setUpMainObservers() {
        super.setUpMainObservers()
        getViewModel().cryptosLiveData.observe(Observer(this@CryptoCurrenciesFragment::observedCryptos))
    }

    private fun observedCryptos(cryptoListState: ViewState<List<CryptoCurrencyUI>>) {
        when (cryptoListState) {
            //todo: Having viewType in recyclerview to display loading at the end ...
            is ViewState.Loading -> binding.swipeRefresh.isRefreshing = cryptoListState.isLoading
            is ViewState.RenderSuccess -> {
                cryptoAdapter.submitList(cryptoListState.output)
            }
            is ViewState.RenderFailure -> displayError(cryptoListState.failure)
        }

    }

    override fun onRefresh() {
        getViewModel().onRefresh()
    }

    override fun onClickCrypto(cryptoCurrencyUI: CryptoCurrencyUI) {
        getViewModel().onSelectCrypto(cryptoCurrencyUI)
    }

}

package com.molotov.cryptocurrency.domain

import androidx.annotation.NonNull
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.molotov.cryptocurrency.data.room.RoomConstants

// todo: having domain free of room but meanwhile create a mapper from UI Side
@Entity(tableName = RoomConstants.TABLE_CRYPTO_CURRENCY)
data class CryptoCurrency(
    @PrimaryKey
    val id: Int,
    val name: String,
    val fullName: String,
    val imageUrl: String,
    val symbol: String,
    val price: Double,
    val indexInResponse: Int,
)

data class CryptoCurrencyDetail(
    val nameSymbol: String,
    val currentVolume: Double,
    val last24Volume: Double,
    val currentVolumeEur: Double,
    val last24VolumeEUR: Double,
)

@Entity(tableName = RoomConstants.TABLE_CRYPTO_CURRENCY_JOIN)
data class CryptoCurrencyJoin(

    @PrimaryKey
    @Embedded(prefix = "crypto_")
    @NonNull val cryptoCurrency: CryptoCurrency,
    @Embedded(prefix = "detail_")
    val cryptoCurrencyDetail: CryptoCurrencyDetail,
)
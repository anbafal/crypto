package com.molotov.cryptocurrency.domain.usecase

import com.molotov.cryptocurrency.common.Result
import kotlinx.coroutines.flow.Flow


abstract class BaseUseCase<out Type : Any, in Params> {

    abstract suspend operator fun invoke(params: Params): Flow<Result<Type>>


    open class NoParams
}
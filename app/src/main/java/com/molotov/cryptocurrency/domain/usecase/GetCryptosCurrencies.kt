package com.molotov.cryptocurrency.domain.usecase

import com.molotov.cryptocurrency.common.Result
import com.molotov.cryptocurrency.domain.CryptoCurrency
import com.molotov.cryptocurrency.domain.repository.ICryptoRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetCryptosCurrencies @Inject constructor(private val cryptoRepository: ICryptoRepository) :
    BaseUseCase<List<CryptoCurrency>, Int>() {
    override suspend fun invoke(params: Int): Flow<Result<List<CryptoCurrency>>> {
        return cryptoRepository.topListCryptos(params)
    }
}
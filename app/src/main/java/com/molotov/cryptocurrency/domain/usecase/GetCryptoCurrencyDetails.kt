package com.molotov.cryptocurrency.domain.usecase

import com.molotov.cryptocurrency.common.Result
import com.molotov.cryptocurrency.domain.CryptoCurrencyJoin
import com.molotov.cryptocurrency.domain.repository.ICryptoRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetCryptoCurrencyDetails @Inject constructor(private val cryptoRepository: ICryptoRepository) :
    BaseUseCase<CryptoCurrencyJoin, GetCryptoCurrencyDetails.Params>() {
    override suspend fun invoke(
        params: Params,
    ): Flow<Result<CryptoCurrencyJoin>> {
        return cryptoRepository.dailyHistorical(params.cryptoId, params.cryptoSymbol)
    }

    class Params(val cryptoId: Int, val cryptoSymbol: String)
}
package com.molotov.cryptocurrency.domain.repository

import com.molotov.cryptocurrency.common.Result
import com.molotov.cryptocurrency.domain.CryptoCurrency
import com.molotov.cryptocurrency.domain.CryptoCurrencyJoin
import kotlinx.coroutines.flow.Flow


interface ICryptoRepository {
    suspend fun topListCryptos(page: Int): Flow<Result<List<CryptoCurrency>>>
    suspend fun dailyHistorical(
        cryptoId: Int,
        cryptoSymbol: String,
    ): Flow<Result<CryptoCurrencyJoin>>
}
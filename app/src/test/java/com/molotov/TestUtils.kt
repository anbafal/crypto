package com.molotov

import java.io.BufferedReader
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader

class MockResponseFileReader(path: String) {
    val content: String

    init {
        val reader = InputStreamReader(this.javaClass.classLoader.getResourceAsStream(path))
        content = reader.readText()
        reader.close()
    }
}


@Throws(IOException::class)
fun readFileWithoutNewLineFromResources(javaClass: Class<out Any>, fileName: String): String {
    var inputStream: InputStream? = null
    try {
        inputStream =
            javaClass.classLoader?.getResourceAsStream(fileName)
        val builder = StringBuilder()
        val reader = BufferedReader(InputStreamReader(inputStream))

        var str: String? = reader.readLine()
        while (str != null) {
            builder.append(str)
            str = reader.readLine()
        }
        return builder.toString()
    } finally {
        inputStream?.close()
    }
}
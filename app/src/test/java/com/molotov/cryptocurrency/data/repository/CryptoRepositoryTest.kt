package com.molotov.cryptocurrency.data.repository

import com.molotov.cryptocurrency.TestCoroutineRule
import com.molotov.cryptocurrency.common.Result
import com.molotov.cryptocurrency.data.api.CryptoCurrencyDetailsEntity
import com.molotov.cryptocurrency.data.api.CryptoCurrencyEntity
import com.molotov.cryptocurrency.data.api.interfaces.IWebService
import com.molotov.cryptocurrency.data.toDomain
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.MockK
import io.mockk.impl.annotations.RelaxedMockK
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.single
import kotlinx.coroutines.test.runBlockingTest
import org.amshove.kluent.any
import org.amshove.kluent.shouldEqual
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class CryptoRepositoryTest {

    @get:Rule
    var mainCoroutineRule = TestCoroutineRule()

    private lateinit var cryptoRepository: CryptoRepository

    @MockK
    private lateinit var webService: IWebService

    @RelaxedMockK
    private lateinit var localStorage: ILocalStorage


    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        cryptoRepository = CryptoRepository(webService, localStorage)
    }


    @Test
    fun `should return list of Crypto when load from webservice succeed`() {
        runBlockingTest {
            coEvery { localStorage.hasCryptosForPage(any()) } returns false
            coEvery { webService.getCryptos(any()) } returns flow {
                emit(Result.OnSuccess(successMockCrypto))
            }
            coEvery { localStorage.getCryptos(any()) } returns flow {
                emit((successMockCrypto.toDomain(1)))
            }

            val result = cryptoRepository.topListCryptos(1).single()

            coVerify { localStorage.saveCryptoCurrencies(successMockCrypto.toDomain(1)) }
            result.shouldEqual(Result.OnSuccess(successMockCrypto.toDomain(1)))

        }
    }

    @Test
    fun `should return list of crypto only from local storage when data already fetch`() {
        runBlockingTest {
            coEvery { localStorage.hasCryptosForPage(any()) } returns true

            coEvery { localStorage.getCryptos(1) } returns flow {
                emit(successMockCrypto.toDomain(1))
            }

            val result = cryptoRepository.topListCryptos(1).single()

            coVerify(exactly = 0) {
                webService.getCryptos(1)
                localStorage.saveCryptoCurrencies(any())
            }

            result.shouldEqual(Result.OnSuccess(successMockCrypto.toDomain(1)))

        }
    }

    //todo: fix it as I fixed above test...same way.
    @Test
    fun `should return list of CryptoDetail when load from webservice succeed`() {
        runBlockingTest {

            coEvery { webService.getDailyHistory("BTC") } returns flow {
                emit(Result.OnSuccess(successMockCryptoDetail))
            }
            // don't matter but have to define it!
            coEvery { localStorage.hasDetailsForCrypto(any()) } returns any(Boolean::class)

            val result = cryptoRepository.dailyHistorical(1, "BTC")
            //todo; investigate
            // coVerify { localStorage.saveCryptoCurrencies(successMockCrypto.toDomain()) }

            result.onCompletion {

                it?.shouldEqual(Result.OnSuccess(successMockCryptoDetail.toDomain("BTC")))
            }
        }
    }

    @After
    fun tearDown() {
    }

    companion object {
        val successMockCrypto = listOf(
            CryptoCurrencyEntity(1, "test", "fulltest", "imageUrl", "symbol", 1.2),
            CryptoCurrencyEntity(2, "test2", "fulltest2", "imageUrl2", "symbol2", 1.3)
        )

        val successMockCryptoDetail = listOf(
            CryptoCurrencyDetailsEntity(1000.90, 10001.90),
            CryptoCurrencyDetailsEntity(1030.90, 1002.90)
        )
    }
}
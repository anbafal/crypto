package com.molotov.cryptocurrency.data.api

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.molotov.MockResponseFileReader
import com.molotov.cryptocurrency.TestCoroutineRule
import com.molotov.cryptocurrency.common.Failure
import com.molotov.cryptocurrency.common.Result
import com.molotov.cryptocurrency.data.api.deserializer.CryptoCurrencyDeserializer
import com.molotov.cryptocurrency.data.api.interfaces.CryptoCompareApi
import com.molotov.cryptocurrency.data.api.utils.IErrorHandler
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.test.runBlockingTest
import org.amshove.kluent.shouldEqual
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import retrofit2.Response


@ExperimentalCoroutinesApi
class RetrofitServiceTest {

    @get:Rule
    var mainCoroutineRule = TestCoroutineRule()

    private lateinit var retrofitService: RetrofitService

    @MockK
    private lateinit var api: CryptoCompareApi

    @MockK
    private lateinit var errorHandler: IErrorHandler

    private lateinit var gson: Gson


    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        retrofitService = RetrofitService(api, errorHandler)
        val gsonBuilder = GsonBuilder()
        gsonBuilder.registerTypeAdapter(CryptoCurrencyResponse::class.java,
            CryptoCurrencyDeserializer())
        gson = gsonBuilder.create()
    }


    @Test
    fun `should return list of Crypto when api call succeed`() {
        mainCoroutineRule.dispatcher.runBlockingTest {

            val json = MockResponseFileReader("crypto_list_success.json").content
            val mockResponse = gson.fromJson(json, CryptoCurrencyResponse::class.java)

            coEvery { api.getTopListCryptos(any()) } returns Response.success(mockResponse)

            val result = retrofitService.getCryptos(1)

            result.onCompletion {
                val cryptoList = mockResponse.entities
                it?.shouldEqual(Result.OnSuccess(cryptoList))
            }
        }
    }

    @Test
    fun `should return ErrNo Data when api not return result anymore`() {
        mainCoroutineRule.dispatcher.runBlockingTest {
            val json = MockResponseFileReader("crypto_list_error.json").content
            val mockResponse = gson.fromJson(json, CryptoCurrencyResponse::class.java)

            coEvery { api.getTopListCryptos(any()) } returns Response.success(mockResponse)

            val result = retrofitService.getCryptos(1)

            result.onCompletion {
                it?.shouldEqual(Result.OnFailed(Failure.Network("Err:No Data")))
            }
        }
    }

    @Test
    fun `should return list of CryptoDetails when api call succeed`() {
        mainCoroutineRule.dispatcher.runBlockingTest {

            val json = MockResponseFileReader("crypto_details_success.json").content
            val mockResponse = gson.fromJson(json, CryptoCurrencyDetailsResponse::class.java)

            coEvery { api.getHistoDay(any()) } returns Response.success(mockResponse)

            val result = retrofitService.getDailyHistory("EUR")

            result.onCompletion {
                val cryptoDetailsList = mockResponse.volumes
                it?.shouldEqual(Result.OnSuccess(cryptoDetailsList))
            }
        }
    }

    @After
    fun tearDown() {

    }
}
package com.molotov.cryptocurrency.features.cryptocurrencies

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import androidx.lifecycle.SavedStateHandle
import com.molotov.cryptocurrency.TestCoroutineRule
import com.molotov.cryptocurrency.common.Failure
import com.molotov.cryptocurrency.common.Result
import com.molotov.cryptocurrency.domain.CryptoCurrency
import com.molotov.cryptocurrency.domain.usecase.GetCryptosCurrencies
import com.molotov.cryptocurrency.features.toUI
import com.molotov.cryptocurrency.features.utils.ViewState
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import io.mockk.verifyOrder
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class CryptoCurrenciesViewModelTest {


    @get:Rule
    var mainCoroutineRule = TestCoroutineRule()

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @MockK
    private lateinit var viewStateObserver: Observer<ViewState<List<CryptoCurrencyUI>>>

    @MockK
    private lateinit var getCryptosCurrencies: GetCryptosCurrencies

    private lateinit var viewModel: CryptoCurrenciesViewModel


    @Before
    fun setup() {
        MockKAnnotations.init(this)
        viewModel = CryptoCurrenciesViewModel(SavedStateHandle(),
            getCryptosCurrencies,
            Dispatchers.Unconfined)
    }

    @Test
    fun `should load list of CryptoCurrencyUI  when succeed`() {

        mainCoroutineRule.dispatcher.runBlockingTest {

            coEvery { getCryptosCurrencies(1) } returns flow {
                emit(Result.OnSuccess(successMockCrypto))
            }

            viewModel.cryptosLiveData.observeForever(viewStateObserver)
            viewModel.loadCryptos()

            verifyOrder {
                viewStateObserver.onChanged(ViewState.Loading(true))
                viewStateObserver.onChanged(ViewState.RenderSuccess(successMockCrypto.toUI()))
                viewStateObserver.onChanged(ViewState.Loading(false))
            }
        }
    }

    @Test
    fun `should return the Failure list of CryptoCurrencyUI  when not succeed`() {

        runBlockingTest {

            coEvery { getCryptosCurrencies(1) } returns flow {
                emit(Result.OnFailed(Failure.AccessDenied))
            }

            viewModel.cryptosLiveData.observeForever(viewStateObserver)
            viewModel.loadCryptos()

            verifyOrder {
                viewStateObserver.onChanged(ViewState.Loading(true))
                viewStateObserver.onChanged(ViewState.RenderFailure(Failure.AccessDenied))
                viewStateObserver.onChanged(ViewState.Loading(false))
            }
        }
    }

    @After
    fun tearDown() {
    }


    companion object {
        val successMockCrypto = arrayListOf(
            CryptoCurrency(1, "test", "fulltest", "/imageUrl", "symbol", 1.2, 0),
            CryptoCurrency(2, "test2", "fulltest2", "/imageUrl2", "symbol2", 1.3, 0)
        )
    }


}
# CryptoCurrency
An app displaying cryptocurrencies and showing details when tapping on one.

Recommended stack
* Kotlin
* Retrofit
* Coroutines & Flow
* Dependency Injection avec Koin
* Room
* Architecture Component (ViewModel, etc.)
* AndroidX
* MVVM
* Clean Architecture

Goal : Create an app displaying cryptocurrencies and showing details when tapping on one
API to use : https://min-api.cryptocompare.com/documentation (See "Historical Daily OHLCV" and "Toplist by Market Cap Full Data")
API KEY : b2a3a2a8f706126d8bd273c4b533d362a1548710256749605b6effa403db0c85

Specifications

* 1st screen (main) :
Display all crypto currencies with their name, logo, symbol and value (€(EUR) conversion)
Paginate with limit at 20 (lazy loading)

Bonus:
Polling every 2 minutes
Display every changes with green/red text (when value is higher/lower than before) for 20 seconds

* 2nd screen (details) :
On click on an item list of the 1st screen, display  :
* crypto currency details : Name, logo, symbol, prive, volume over 24h...
* converter with user input to get currency info : I Want to Buy for {UserInput}€ I will have {ComputedValue} Of Crypto Money
Converter with user input to buy fake currency ("I Want to Buy for {UserInput}€ I will have {ComputedValue} Of {Crypto Money}"...)

* Misc
The app can work offline if the data was already loaded once.
Bonus
The cache validity is 20 minutes.